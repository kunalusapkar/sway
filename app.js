const path = require('path');
const express= require("express");
const morgan = require('morgan');
const rateLimit = require('express-rate-limit')
const helmet = require('helmet')
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean')
const hpp = require('hpp')
const cookieParser = require('cookie-parser')
const AppError = require('./util/appError')
const tourRouter = require('./routes/tourRoutes')
const userRouter = require('./routes/userroutes')
const reviewRouter = require('./routes/reviewroutes')
const viewRouter = require('./routes/viewRoutes')
const catRouter = require('./routes/categoryroutes')
const bookingRouter = require('./routes/bookingroutes')
const compression = require('compression')
const errController = require('./controllers/errorController')
const bookingController = require('./controllers/bookingcontroller')
const bodyParser = require('body-parser');

const app = express();

// middlewares

// Set security http headers
app.use(helmet())

app.set('views', path.join(__dirname, 'views'));
app.set("view engine", "ejs");
app.use(express.static(path.join(__dirname, "public")));

if(process.env.NODE_ENV === 'development'){
    app.use(morgan('dev'))
}
// Rate limiter no req to be done max 100 specified
const limiter = rateLimit({
  max:100,
  windowMs:60 * 60 * 1000,
  message:'Too many request from this IP Please try again later'
})
app.use('/api',limiter)
// app.use(express.raw())
app.post('/webhook-checkout', bodyParser.raw({ type: 'application/json' }),bookingController.webhookCheckout)

app.use(
  express.json({
    limit: '10kb'
  })
);
// for parse cookie
app.use(cookieParser())
app.use((req,res,next)=>{
  console.log(req.cookies)
  console.log(process.env.NODE_ENV)
  next()
})
// Data sanitization against NOsql query injection
// data sanitazation against nosql query inject
app.use(mongoSanitize());


// Data sanitization against XSS
app.use(xss());

app.use(express.static(`${__dirname}/public`))

// prevent parameter pollution
  hpp({
    whitelist: [
      'duration',
      'ratingsQuantity',
      'ratingsAverage',
      'maxGroupSize',
      'difficulty',
      'price'
    ]
  })
app.use(compression())
// routes

app.use('/',viewRouter)
app.use('/api/v1/users',userRouter)
app.use('/api/v1/tours',tourRouter)
app.use('/api/v1/reviews',reviewRouter)
app.use('/api/v1/category',catRouter)
app.use('/api/v1/bookings',bookingRouter)
app.all('*',(req,res,next)=>{
 
   next(new AppError(`Cannot find requested url ${req.originalUrl}`,404))
})

app.use(errController)
// server
module.exports = app;