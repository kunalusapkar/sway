const mongoose = require('mongoose')
const dotenv = require('dotenv')
// process.on('uncaughtException',err=>{
//     console.log("UNHANDLED EXCEPTION ")
//     console.log(err.name,err.message)
//     server.close(()=>{
//         process.exit(1)
//     })
// })
dotenv.config({path:'./config.env'})
const app = require('./app');
// const port = 3000 ||process.env.PORT 
const port = process.env.PORT 
const DB = process.env.DATABASE.replace(
    '<PASSWORD>',
    process.env.DATABASE_PASSWORD
)
mongoose.connect(DB,{
    useNewUrlParser:true,
    useCreateIndex:true,
    useFindAndModify:true,
}).then(con=>console.log('DB succesfully connected'));


const server = app.listen(port, ()=>{
    console.log(`Running on port ${port}`)
})

process.on('unhandledRejection',err=>{
    console.log("Errorrr------>",err.name,err.message)
    console.log("UNHANDLED REJECTION")
    server.close(()=>{
        process.exit(1)
    })
})

process.on('SIGTERM', () => {
    console.log("Sigterm recived shutting down gracefully");
    server.close(() => {
      console.log("Process terminated");
    });
  });

