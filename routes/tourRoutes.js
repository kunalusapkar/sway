const express= require("express");
const tourController = require("../controllers/tourController");
const authController = require('../controllers/authController') 
// const reviewController = require('./../controllers/reviewcontroller')
const reviewRouter = require('./reviewroutes')
const router = express.Router();

// router.route('/:tourId/reviews')
// .post(authController.protect,authController.restrictTo('user'),reviewController.createReview)

router.use('/:tourId/reviews',reviewRouter)
// authController.restrictTo('admin')
router.route('/top-5-cheap').get(tourController.topTour,tourController.getAllTours)
router.route('/').get(tourController.getAllTours).post(authController.protect,authController.restrictTo('admin'),tourController.createTour);
router.route('/:id').get(tourController.getTour).patch(authController.protect,authController.restrictTo('admin'),
tourController.uploadTourImages,tourController.resizeTourImages,tourController.updateTour)
.delete(authController.protect,authController.restrictTo('admin'),tourController.deleteTour);
router.route('/month-plan/:year').get(tourController.monthlyPlan)




module.exports = router