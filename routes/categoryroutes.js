const express= require("express");
const catController = require('./../controllers/categorycontroller')
const authController = require('../controllers/authController') 
const router = express.Router()

router.route('/').post(catController.createCategory).get(catController.getAllCategory)

module.exports = router