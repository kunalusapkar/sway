const express = require('express')
const authController = require('./../controllers/authController')
const userController = require('./../controllers/usercontoller')
const multer = require('multer')

const upload = multer({dest:'public/img/users'})
const router = express.Router();

router.post('/signup', authController.signup);
router.post('/login', authController.login);
router.get('/logout', authController.logOut);
router.post('/forgotpassword', authController.forgotPassword);
router.patch('/resetpassword/:token', authController.resetPassword);
// Protect all routes middleware
router.use(authController.protect)
router.patch('/updatemypassword',authController.updatePassword)
router.patch('/updateme',authController.uploadUserPhoto,authController.resizeUserPhoto,authController.updateMe)
router.delete('/deleteme',authController.deleteMe)
router
    .route('/')
    .get(userController.getAllUsers)
router.get('/me',authController.protect,userController.getMe,userController.getUser)

module.exports = router