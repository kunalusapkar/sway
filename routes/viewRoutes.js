const express = require('express')
const router = express.Router()
const viewController = require('../controllers/viewController')
const authController = require('../controllers/authController')
const bookingController = require('../controllers/bookingcontroller')

// router.use(authController.isLoggedIn)
router.get('/login',authController.isLoggedIn,viewController.login)
router.get('/register',viewController.register)
router.get('/',authController.isLoggedIn,viewController.home)
router.get('/search',authController.isLoggedIn,viewController.search) 
router.get('/tour/:slug',authController.isLoggedIn,viewController.searchDetails)
router.get('/me',authController.protect ,viewController.profile)
router.get('/my-tours',authController.protect ,viewController.getMyTours)

module.exports = router