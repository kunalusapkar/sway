const mongoose = require('mongoose');
const Tour = require('./tourmodel')
const reviewSchema = new mongoose.Schema(
  {
    review: {
      type: String,
      required: [true, 'A review cannot be empty']
    },
    rating: {
      type: Number,
      min: 1,
      max: 5
    },
    createdAt: {
      type: Date,
      default: Date.now(),
      select: false
    },
    tour: {
      type: mongoose.Schema.ObjectId,
      ref: 'Tour',
      required: [true, 'Review must belong to tour']
    },
    user: {
      type: mongoose.Schema.ObjectId,
      ref: 'User',
      required: [true, 'Review must belong to user']
    }
  },
  {
    toJSON: {
      virtuals: true
    },
    toObject: {
      virtuals: true
    }
  }
);

// to create one review on one tour by one user 
reviewSchema.index({ tour: 1, user: 1 }, { unique: true });
reviewSchema.pre(/^find/, function(next) {
  // this.populate({
  //     path: 'tour',
  //     select: 'name'
  // }).populate({
  //     path: 'user',
  //     select: 'name photo'
  // });
  this.populate({
    path: 'user',
    select: 'name photo'
  });
  next();
});
// create a function(static method)
reviewSchema.statics.calcAverageRatings = async function(tourId) {
  const stats =  await this.aggregate([
     {
       $match:{tour:tourId}
     },
     {
      //  demo nRating:1 avgRating:3 nrating:2 avgrating:4 nrating is number of rating given 
       $group:{
         _id:'$tour',
         nRating:{$sum:1},
         avgRating:{$avg:'$rating'}
       }
     }
   ])
   if(stats.length > 0){
    await Tour.findByIdAndUpdate(tourId,{
      ratingsQuantity:stats[0].nRating,
      ratingsAverage:stats[0].avgRating
    })
   }else{
    await Tour.findByIdAndUpdate(tourId,{
      ratingsQuantity:0,
      ratingsAverage:4.5
    })
   }
   
}
// and call middleware to run schema 
reviewSchema.post('save',function(){
  // Review.calcAverageRatings(this.tour) points to current model
  this.constructor.calcAverageRatings(this.tour)
})

// to update and delete

reviewSchema.pre(/^findOneAnd/,async function(next){
  this.r = await this.findOne()
  console.log(r)
})
reviewSchema.post(/^findOneAnd/,async function(next){
  // got data from line 84
  await this.r.constructor.calcAverageRatings(this.r.tour)
})
const Review = mongoose.model('Review', reviewSchema);

module.exports = Review;