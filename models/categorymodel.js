const mongoose = require('mongoose')
const slugify = require('slugify');
const validator = require('validator');

const categorySchema = mongoose.Schema({
    name:{
        type:String,
        required:[true,'A category mush have name'],
        unique:true,
    },
    category_id:{
        type:Number,
        required:[true,'A category must have id']
    },
    slug:String
})

const Category = mongoose.model('Category',categorySchema)
module.exports = Category