const mongoose = require('mongoose')
const slugify = require('slugify');
const validator = require('validator');
const Category = require('./categorymodel')
const tourSchema = new mongoose.Schema({
    name:{
        type:String,
        required:[true,'A tour must have name'],
        unique:true,
        maxlength:[40,'Tour must hav max 40 char'],
        minlength:[10,'A tour must have 10 min char'],
        // validate:[validator.isAlpha,'Tour name must contain character']
    },
    duration: {
        type: Number,
        required: [true, 'A tour must have a duration']
    },
    maxGroupSize: {
        type: Number,
        required: [true, 'A tour must have a group size']
    },
    difficulty: {
        type: String,
        required: [true, 'A tour must have a difficulty'],
        enum: {
            values: ['easy', 'medium', 'difficult'],
            message: 'Difficulty is either: easy, medium, difficult'
        }
    },
    
    ratingsAverage: {
        type: Number,
        default: 4.5,
        min: [1, 'Rating must be above 1.0'],
        max: [5, 'Rating must be below 5.0'],
        set: val => Math.round(val * 10) / 10 //4.666666 46.6666 47 4.7
    },
    ratingsQuantity: {
        type: Number,
        default: 0
    },
    price:{
        type:Number,
        required:[true,'A tour must have price']
    },
    priceDiscount: {
        type: Number,
        validate: {
            validator: function (val) {
                // this only points to current doc on NEW document creation
                return val < this.price;
            },
            message: 'Discount price ({VALUE}) should be below regular price'
        }
    },
    summary: {
        type: String,
        trim: true,
        required: [true, 'A tour must have a description']
    },
    description: {
        type: String,
        trim: true
    },
    imageCover: {
        type: String,
        required: [true, 'A tour must have a cover image']
    },
    images: [String],
    slug:String,
    createdAt: {
        type: Date,
        default: Date.now(),
        select: false
    },
    startDates: [Date],
    category:[
        {
          type:mongoose.Schema.ObjectId,
          ref:'Category',
          field: 'category_id'
        }
    ],
    secretTour:{
        type:Boolean
    },
    city:{
        type:String,
        maxlength:[20,'Location must have max 20 char']
    },
    category_name:[String]
},{
    toJSON:{virtuals:true},
    toObject:{virtuals:true}
})
tourSchema.virtual('durationinweeks').get(function(){
    return this.duration/7;
})
// indexing

// tourSchema.index({price:1})
tourSchema.index({price:1,ratingsAverage:-1})
tourSchema.index({slug:1})
// virtual populate reviews for tour
tourSchema.virtual('reviews',{
    ref:'Review',
    foreignField:'tour',
    localField:'_id'
})
tourSchema.pre(/^find/,function(next){
    this.populate({
        path:'category',
        select:'-_id -__v'
    })
    next()
})
// tourSchema.methods.catMap = async function(catName){
//     var cat_Data = Category.find(category_id)
//     console.log(cat_Data)
// }
// tourSchema.statics.catData = async function(){
//     console.log("presave working")
//     const catPromises = this.category.map(async id=>await Category.findById(id,'-_id -__v -category_id'))
//     category_name = await Promise.all(catPromises)
//     var data2;
//     // this.category_name = JSON.stringify(category_name.name)
//     category_name.forEach(data=>{
//         data2 = data
//     })
//     console.log(data2)
//     return "data2"
//     // console.log(category_name[1].name)
// } 

// tourSchema.pre('save',async function(next){
//     console.log("presave working")
//     const catPromises = this.category.map(async id=>await Category.findById(id,'-_id -__v -name'))
//     var category_id = await Promise.all(catPromises)
//     console.log(category_id.toString().replace(/category_id:|[{}]|'/g,""))
//     var cat_Data = category_id.toString().replace(/name:|[{}]|'/g,"").trim()
//     this.category_name = cat_Data
//     next()
// })
// // will not work on insertmany
// tourSchema.pre('save',function(next){
//     // console.log("this is before saving")
//     this.slug = slugify(this.name,{lower:true});
//     next();
// })
// tourSchema.post('save',function(doc,next){
//     // console.log("this is before saving")
//     console.log(doc);
//     next();
// })
// tourSchema.pre('find',function(doc,next){
//     // console.log("this is before saving")
//     this.find({secretTour:{$ne:true}})
//     console.log(doc);
//     next();
// })
const Tour = mongoose.model('Tour',tourSchema);
module.exports = Tour;
