// type is success or error
export const hideAlert  = () => {
    const el = document.querySelector('.alert');
    if (el) el.parentElement.removeChild(el);
}

export const showAlert = (type,msg)=>{
    // to hide the previous alerts
    hideAlert()
    const markup = `<div class="alert alert-dismissible alert-${type}">
    <strong>${msg}
  </div>`
  document.querySelector('body').insertAdjacentHTML('afterbegin',markup)
  window.setTimeout(hideAlert,5000)
}