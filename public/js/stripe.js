import '@babel/polyfill'
import axios from 'axios'
import {
    showAlert
} from './alerts'
const stripe = Stripe('pk_test_wCRT98QMNlUTRBfywGolspY000sk0p4XPW')


export const bookTour = async tourId => {
    try{
    //1) get check out session from API
        const session = await axios(`/api/v1/bookings//checkout-session/${tourId}`)
    // 2)Create checkout form +charge credit card
    await stripe.redirectToCheckout({
        // session coming from response
        sessionId: session.data.session.id
    });

    }catch(err){
        console.log(err)
        showAlert('primary',err)
    }
}