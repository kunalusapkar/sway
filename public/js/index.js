import {login,logout} from './login'
import {updateUser} from './update_user'
import {bookTour} from './stripe'

const loginForm = document.querySelector('.form')
const logoutBtn = document.querySelector('.logout')
const updateUserData = document.querySelector('.profile')
const updatePassword = document.querySelector('.profile-password')
const bookBtn = document.getElementById('book-tour')
if(loginForm){
    loginForm.addEventListener('submit',e=>{
        e.preventDefault()
        const email = document.getElementById('email').value
        const password = document.getElementById('password').value
        login(email,password)
    })
}

if(updateUserData){
    updateUserData.addEventListener('submit',e=>{
    e.preventDefault()
    const form = new FormData()
    form.append('name',document.getElementById('name').value)
    form.append('email',document.getElementById('email').value)
    form.append('photo',document.getElementById('photo').files[0])
    // const name = document.getElementById('name').value
    // const email = document.getElementById('email').value
    updateUser(form,'data')
})
}

if(updatePassword){
    updatePassword.addEventListener('submit',async e=>{
    e.preventDefault()
    // document.querySelector('.btn-save-password').textContent = "Updating...."
    const passwordCurrent = document.getElementById('current_password').value
    const password = document.getElementById('password').value
    const passwordConfirm = document.getElementById('confirm_password').value
    await updateUser({passwordCurrent,password,passwordConfirm},'password')
    // document.querySelector('.btn-save-password').textContent = 'Update Password'
    document.getElementById('current_password').value = ''
    document.getElementById('password').value = ''
    document.getElementById('confirm_password').value = ''
})
}

if(logoutBtn) logoutBtn.addEventListener('click',logout)

if(bookBtn)
bookBtn.addEventListener('click',e=>{
    const { tourId } = e.target.dataset
    bookTour(tourId)
})