import '@babel/polyfill'
import axios from 'axios'
import {
    showAlert
} from './alerts'

// type is either password or data
export const updateUser = async(data,type)=>{
    try{
        const url = type === 'password'?'/api/v1/users/updatemypassword':'/api/v1/users/updateme'
        const res = await axios({
            method:'PATCH',
            url,
            data
        })
        if(res.data.status === 'success'){
            showAlert('success',`${type} updated succcesfully`)
            // window.setTimeout(()=>{
            //     location.assign('/')
            // },1500)
        }
    }catch(err){
        showAlert('primary',err.response.data.message) 
    }
}