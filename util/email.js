const nodemailer = require('nodemailer')
const htmlToText = require('html-to-text')
const ejs = require('ejs')

module.exports = class Email{
    constructor(user,url){
        this.to = user.email;
        this.firstName  = user.name.split(' ')[0];
        this.url = url;
        this.from = 'Swayourway<sway@sway.io>';
    }
    newTransport(){
      if(process.env.NODE_ENV === 'production'){
        // sendgdrid
        return nodemailer.createTransport({
            service:'SendGrid',
            auth:{
                user:process.env.SENDGRID_USERNAME,
                pass:process.env.SENDGRID_PASSWORD
            }
        })
      }  
       return nodemailer.createTransport({
            host:process.env.EMAIL_HOST,
            port:process.env.EMAIL_PORT,
            auth:{
                user:process.env.EMAIL_USERNAME,
                pass:process.env.EMAIL_PASSWORD
            }
        })
      
    }
    
    async send(template,subject){
        let templateData = {
            firstName:this.firstName,
            url:this.url,
            subject
        }
        let emailTemplate = `${__dirname}/../views/emails/${template}.ejs`
        // 1)Render html template
        const html = ejs.renderFile(emailTemplate,templateData,async (err,html)=>{
            if (err) return err;
            // 2)Define email option
        const mailOptions = {
                from:this.from,
                to:this.to,
                subject,
                html,
                text:htmlToText.fromString(html)
            }
            // create a transport and send mail
        await this.newTransport().sendMail(mailOptions)
        })
     }
    async sendWelcome(){
        await this.send('welcome','Welcome to swayourway')
    }
    async sendPasswordReset(){
        await this.send('passwordreset','Valid For 10 minutes')
    }
}


// const sendEmail = async options =>{
//     // define the email options
  
//     // Actually send email

//     await transporter.sendMail(mailOptions)
// }