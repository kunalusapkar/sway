const fs = require('fs');
const Tour = require('../models/tourmodel');
const catchAsync = require('../util/catchAsync')
const AppError = require('../util/appError')
const factory = require('./handlerfactory')
const multer = require('multer')
const sharp = require('sharp')
// route handlers

// exports.createTour = catchAsync(async (req,res,next)=>{
//     const newTour = await Tour.create(req.body);
//     console.log("inside ct")
//     res.status(201).json({
//         status:'success',
//         data:{
//             tour:newTour
//         }
//     })
    
// })
const multerStorage = multer.memoryStorage()
// Check file type for eg jpeg
const multerFilter = (req,file,cb)=>{
    if(file.mimetype.startsWith('image')){
        cb(null,true)
    }else{
        cb(new AppError('Not an image!Please upload only images'),false)
    }
}
const upload = multer({
    storage:multerStorage,
    fileFilter:multerFilter
})

exports.uploadTourImages = upload.fields([
    {name:'imageCover',maxCount:1},
    {name:'images',maxCount:3}
])

exports.resizeTourImages = catchAsync(async(req,res,next)=>{
    if(!req.files.imageCover || !req.files.images) return next()
    req.body.imageCover = `tour-${req.params.id}-${Date.now()}-cover.jpeg`
    // Cover image
    await sharp(req.files.imageCover[0].buffer).resize(2000,1333)
    .toFormat('jpeg').jpeg({quality:90}).
    toFile(`public/img/tours/${req.body.imageCover}`)
    // 2)multiple tour images
    req.body.images = []
    await Promise.all(req.files.images.map(async(file,i)=>{
        const fileName = `tour-${req.params.id}-${Date.now()}-${i+1}.jpeg`
        await sharp(file.buffer).resize(2000,1333)
        .toFormat('jpeg').jpeg({quality:90}).
        toFile(`public/img/tours/${fileName}`)

        req.body.images.push(fileName)
    })
    )
    next()
})
// upload.array('images',5) if there was no imagecover req.files
// upload.single('image) req.file
exports.createTour = factory.createOne(Tour)
exports.topTour = catchAsync(async(req,res,next)=>{
        req.query.limit = '5';
        req.query.sort = '-ratingsAverage,price';
        req.query.fields = 'name,price,ratingsAverage,summary,difficulty';
        next();
    
})
// exports.getAllTours = catchAsync(async(req,res,next)=>{
//         // execute query
//         const features = new APIFeatures(Tour.find(),req.query)
//         .filter()
//         .sort()
//         .limitFields()
//         .pagination()
//         const tours = await features.query
//         res.status(200).json({
//             status:'success',
//             results:tours.length,
//             data:{
//                 tours
//             }
//         })
     
// });
exports.getAllTours = factory.getAll(Tour)


// exports.getTour = catchAsync(async (req,res,next)=>{
//         const tours = await Tour.findById(req.params.id).populate('reviews')
//         if(!tours){
//             return next(new AppError('No tour found with that ID',404))
//         }
//         res.status(200).json({
//             status:'success',
//             results:tours.length,
//             data:{
//                 tours
//             }
//         })
        
// })
exports.getTour = factory.getOne(Tour,{path:'reviews'})

// exports.updateTour = catchAsync(async (req,res,next)=>{
//         const tour = await Tour.findByIdAndUpdate(req.params.id,req.body,{
//             new:true,
//             runValidators:true
//         })
//         if(!tours){
//             return next(new AppError('No tour found with that ID',404))
//         }
//         res.status(200).json({
//             status:'success',
//             data:{
//                 tour
//             }
//     })

// })
// donot use for updating password because findbyidandupdate will not activate mongo middleware functions
exports.updateTour = factory.updateOne(Tour)
exports.deleteTour = factory.deleteOne(Tour)
// exports.deleteTour = catchAsync(async (req,res,next)=>{
//         const tour = await Tour.findByIdAndDelete(req.params.id)
//         if(!tour){
//             return next(new AppError('No tour found with that ID',404))
//         }
//         res.status(200).json({
//             status:'success',
//             data:{
//                 message:'Deleted succesfully'
//             }
//     })
    
// })

exports.monthlyPlan = catchAsync(async(req,res,next)=>{
        const year = req.params.year * 1;
        const plan = await Tour.aggregate([
            {
                $unwind: '$startDates'
            },
            {
            $match:{
                startDates:{
                    $gte: new Date(`${year}-01-01`),
                    $lte: new Date(`${year}-12-31`)
                }
            }
            },
            {
                $group:{
                    _id:{$month:'$startDates'},
                    numTourStarts:{$sum:1},
                    tours:{$push:'$name'}
                } 
            },
            {
                $addFields:{month:'$_id'}
            },
            {
                $project:{
                    _id:0
                }
            },
            {
                $sort:{
                    numTourStarts:-1
                }
            },
            // {
            //     $limit:6
            // }
        ])
        res.status(200).json({
            status:'success',
            results:plan.length,
            data:plan
        })

   
})