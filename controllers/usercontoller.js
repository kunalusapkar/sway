const User = require('./../models/usermodel');
const catchAsync = require('./../util/catchAsync');
const AppError = require('./../util/appError')
const factory = require('./handlerfactory')


exports.getAllUsers =  catchAsync(async(req, res) => {
    const users = await User.find()
    res.status(200).json({
      status: 'success',
      results:users.length,
      data:{
          users
      }
    });
  })

  exports.getUser = factory.getOne(User);
  exports.getMe = (req,res,next)=>{
    req.params.id = req.user.id
    next()
  }