const AppError = require('./../util/appError')
const handleCastErrorDB = err =>{
  const message = `Invalid ${err.path}:${err.value}`
  return new AppError(message,400)
}
const handleDuplicateFields = err =>{
  const value = err.errmssg.match(/(["'])(\\?.)*?\1/)[0];
  const message = `Duplicate field value ${value} Please use another value`
  return new AppError(message,400)
}
const handleValidationErrorDB = err=>{
  const errors = Object.values(err.errors).map(el=>el.message)
  const message = `Invalid input ${errors.join('. ')}`
  return new AppError(message,400)
}

const handleJsonWebTokenError = () => new AppError('Invalid token.Please login again',401)
const handleTokenExpiredError = () => new AppError('Your token has expired.Please login again',401)

const sendErrDev = (err,req,res)=>{
  // for Api
  if(req.originalUrl.startsWith('/api')){
    res.status(err.statusCode).json({
      status:err.status,
      message:err.message,
      stack:err.stack,
      error:err
    })
  }else{
    // for frontend
    res.status(err.statusCode).render('pages/error',{
      title:'Something went wrong',
      msg:err.message
    })
  }
}
const sendErrProd = (err,req,res)=>{
  // for api
  if(req.originalUrl.startsWith('/api')){
      if(err.isOperational){
        return res.status(err.statusCode).json({
          status:err.status,
          message:err.message
        })
        return res.status(500).json({
          status:'error',
          message:'Something went wrong in server try after sometime'
        })
  }
    // for website
    if(err.isOperational){
      return res.status(err.statusCode).json({
        status:err.status,
        message:err.message
      })
    }
      return res.status(err.statusCode).render('pages/error',{
        title:'Something went wrong',
        msg:'Please try ahgain later'
      })
  
}
}

module.exports = (err,req,res,next)=>{
    err.statusCode = err.statusCode || 500;
    err.status = err.status ||"error"
    if(process.env.NODE_ENV === 'development'){
      sendErrDev(err,req,res)
    }else if(process.env.NODE_ENV === 'production'){
      let error = { ...err }
      error.message = err.message
      if(error.name === 'CastError') error =  handleCastErrorDB(error)
      if(error.name === 11000) error =  handleDuplicateFields(error)
      if(error.name === 'ValidationError') error =  handleValidationErrorDB(error)
      if(error.name === 'JsonWebTokenError') error =  handleJsonWebTokenError()
      if(error.name === 'TokenExpiredError') error =  handleTokenExpiredError()
      sendErrProd(error,req,res)
    }
  }