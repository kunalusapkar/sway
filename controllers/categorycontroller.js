const Category = require('./../models/categorymodel')
const catchAsync = require('../util/catchAsync')
const AppError = require('../util/appError')
const factory = require('./handlerfactory')


exports.createCategory = factory.createOne(Category)

exports.getAllCategory = factory.getAll(Category)