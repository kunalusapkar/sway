const catchAsync = require('./../util/catchAsync');
const AppError = require('./../util/appError')
const Tour = require('../models/tourmodel')
const Bookings = require('../models/bookingmodel')

exports.home = catchAsync(async (req, res, next) => {
    res.status(200).render('pages/home');
});
exports.login = catchAsync(async (req, res, next) => {
    res.status(200).render('pages/login');
});
exports.register = catchAsync(async (req, res, next) => {
    res.status(200).render('pages/register');
});
exports.search = catchAsync(async (req, res, next) => {
    const tours = await Tour.find()
    res.status(200).render('pages/search',{
        title:'All Tours',
        tours
    });
});
exports.searchDetails = catchAsync(async (req, res, next) => {
    const tour = await Tour.findOne({slug:req.params.slug})

    if(!tour){
        return next(new AppError('There is no tour with this name',404))
    }
    res.status(200).render('pages/search_details',{
        title:`Swayourway|${tour.title}`,
        tour
    });
});

exports.profile = catchAsync(async (req, res, next) => {
    
    res.status(200).render('pages/profile',{
        title:"Your account details"
    });
});

exports.getMyTours = catchAsync(async(req,res,next)=>{
    //find all bookings
    const bookings = await Bookings.find({user:req.user.id})
    
    // find tours with returned IDS
    const tourIDs = bookings.map(el=>el.tour)
    const tours = await Tour.find({_id:{$in:tourIDs}})

    res.status(200).render('pages/mybookings',{
        title:'Booked Tours',
        tours
    });
})