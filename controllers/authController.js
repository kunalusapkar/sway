const crypto = require('crypto')
const {promisify} = require('util')
const User = require('./../models/usermodel')
const catchAsync = require('./../util/catchAsync');
const AppError = require('./../util/appError');
const Email = require('./../util/email');
const jwt = require('jsonwebtoken');
const multer = require('multer')
const sharp = require('sharp')


// const multerStorage = multer.diskStorage({
//     destination:(req,file,cb)=>{
//         cb(null,'public/img/users')
//     },
//     filename:(req,file,cb)=>{
//         // user-3232323-ts.jpeg
//         const ext = file.mimetype.split('/')[1]
//         cb(null,`user-${req.user.id}-${Date.now()}.${ext}`)
//     }
// })
const multerStorage = multer.memoryStorage()
// Check file type for eg jpeg
const multerFilter = (req,file,cb)=>{
    if(file.mimetype.startsWith('image')){
        cb(null,true)
    }else{
        cb(new AppError('Not an image!Please upload only images'),false)
    }
}
const upload = multer({
    storage:multerStorage,
    fileFilter:multerFilter
})
exports.uploadUserPhoto = upload.single('photo')
exports.resizeUserPhoto = catchAsync(async(req,res,next)=>{
    if(!req.file) return next()
    req.file.filename = `user-${req.user.id}-${Date.now()}.jpeg`
    await sharp(req.file.buffer).resize(500,500)
    .toFormat('jpeg').jpeg({quality:90}).
    toFile(`public/img/users/${req.file.filename}`)

    next()

})
const filterObj = (obj, ...allowedFields) => {
    const newObj = {};
    Object.keys(obj).forEach(el => {
      if (allowedFields.includes(el)) newObj[el] = obj[el];
    });
    return newObj;
  };

const signToken = id =>{
    return jwt.sign({id},process.env.JWT_SECRET,{
        expiresIn:process.env.JWT_EXPIRES_IN
    })
}
// Cookie option

const createSendToken = (user,statusCode,res)=>{
    const token = signToken(user._id)
    // to send the cookie
    const cookieOptions = {
        expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
        httpOnly:true
    }
  if(process.env.NODE_ENV === 'production') cookieOptions.secure = true
    res.cookie("jwt",token,cookieOptions)
    user.password = undefined
    res.status(statusCode).json({
        status:'success',
        token,
        data: {
            user
        }
    })
}




exports.signup = catchAsync(async(req,res,next)=>{
    const newUser = await User.create({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        passwordConfirm: req.body.passwordConfirm,
        passwordChangedAt: req.body.passwordChangedAt,
        role: req.body.role
    });
    await new Email(newUser,0).sendWelcome()
    createSendToken(newUser,201,res)
})
exports.login = catchAsync(async(req,res,next)=>{
   const {email,password} = req.body 
    // 1) Check if email and password exists
        if(!email || !password){
           return next( new AppError('Please provide email and password',400))
        }
    // 2) check if user exists and password is correct select('+password') because we have use select:false that hides from showing to user and created instance correctPassword in user model to vertify password 
        const user = await User.findOne({email}).select('+password')
        if(!user || !(await user.correctPassword(password,user.password))){
            return next(new AppError('Incorrect email or password',401))
        }
    // 3) if everything is ok, send token to client
    createSendToken(user,200,res)
})

exports.protect = catchAsync(async(req,res,next)=>{
    // 1) Getting token and check of its there
    let token
        if(req.headers.authorization && req.headers.authorization.startsWith('Bearer')){
            token = req.headers.authorization.split(' ')[1]
        }else if(req.cookies.jwt){
            token = req.cookies.jwt
        }

    // console.log(token)
    if(!token){
        return next(new AppError("You are not logged in please login",401))
    }
    // 2)Vertification of token
    const decoded = await promisify(jwt.verify)(token,process.env.JWT_SECRET)
    // 3)check if user still exists
    const freshUser = await User.findById(decoded.id)
    if(!freshUser){
        return next(new AppError('The user belonging to this token doesnot exists',401))
    }

    // 4)check if user change password after token wass issued
    if(freshUser.changePasswordAfter(decoded.iat)){
        return next(new AppError('User recently changed password!Please login again',401)) 
    }
    // Grant access to protected route
    res.locals.user = freshUser
    req.user = freshUser
    next()
})

exports.restrictTo = (...roles)=>{
    return(req,res,next) => {
        // explaination roles ['admin'] roles ="user" It will check the array if it has permission it will give access
        // req.user comes from line 73
        if(!roles.includes(req.user.role)){
            return next(new AppError('You donot have permission to perform this action',403))
        }
    next();
    }
}
exports.forgotPassword = catchAsync(async (req,res,next)=>{
    // Get user based on post email
    const user = await User.findOne({email:req.body.email})
    if(!user){
        return next(new AppError('No auser with this email address',404))
    }
    // Generate random token
    const resetToken = user.createPasswordResetToken();
    // validateBeforeSave:false remove validation that is applied
    await user.save({validateBeforeSave:false})
    // Send it to user email
    
    try{
        const resetURL = `${req.protocol}://${req.get(
        'host'
      )}/api/v1/users/resetpassword/${resetToken}`;
        await new Email(user,resetURL).sendPasswordReset()
        res.status(200).json({
            status:'succes',
            message:'Token send to email'
        })
    }catch(e){
        user.passwordResetToken = undefined;
        user.passwordResetExpires = undefined;
        await user.save({
            validateBeforeSave: false
        });
        return next(new AppError('There was error in sending email please try again later',500))
    }
      
})


exports.resetPassword = catchAsync(async (req,res,next)=>{
    // 1) Get user based on token
    const hashedToken = crypto.createHash('sha256').update(req.params.token).digest('hex')

    const user = await User.findOne({passwordResetToken:hashedToken,passwordResetExpires:{$gt:Date.now()}})

    // 2)if token has not expired and there is the user set the new password
    if(!user){
        return next(new AppError('Token is invalid or expired',400))
    }
    user.password = req.body.password;
    user.passwordConfirm = req.body.passwordConfirm
    user.passwordResetToken = undefined
    user.passwordResetExpires = undefined
    await user.save();
    // 3)Update change passwordAt for the user


    // 4 logeed the user in and send JWT
    createSendToken(user,200,res)
})


// Change user data after login functionality

exports.updatePassword = catchAsync(async (req,res,next)=>{
    //1) get user from collection
    const user = await User.findById(req.user.id).select('+password')

    //2) check if posted current password is correct
    if(!(await user.correctPassword(req.body.passwordCurrent,user.password))){
        return next(new AppError('Your current password is wrong',401))
    }
    // 3) If so update password
    user.password = req.body.password
    user.passwordConfirm = req.body.passwordConfirm
    await user.save();
    // User.findbyid will not work as intended (their middleware and validstion will not work)
    // 4)log user in send JWT
    createSendToken(user,200,res)
})

exports.updateMe = catchAsync(async (req,res,next)=>{
    // 1)Create error id user posts password data
      // 1) Create error if user POSTs password data
  if (req.body.password || req.body.passwordConfirm) {
    return next(
      new AppError(
        'This route is not for password updates. Please use /updateMyPassword.',
        400
      )
    );
  }

  // 2) Filtered out unwanted fields names that are not allowed to be updated
  const filteredBody = filterObj(req.body, 'name', 'email');
  if(req.file) filteredBody.photo = req.file.filename
  // 3) Update user document
  const updatedUser = await User.findByIdAndUpdate(req.user.id, filteredBody, {
    new: true,
    runValidators: true
  });

  res.status(200).json({
    status: 'success',
    data: {
      user: updatedUser
    }
  });
})

exports.deleteMe = catchAsync(async(req,res,next)=>{
    await User.findByIdAndUpdate(req.user.id,{active:false})
    res.status(204).json({
        status:'success',
        data:null
    })
})


exports.isLoggedIn = catchAsync(async(req,res,next)=>{
    console.log(req.cookies.jwt)
    // 1) Getting token and check of its there
    if(req.cookies.jwt){
        try{
        // 2)Vertification of token
        const decoded = await promisify(jwt.verify)(req.cookies.jwt,process.env.JWT_SECRET)
        // 3)check if user still exists
        const freshUser = await User.findById(decoded.id)
        if(!freshUser){
            return next()
        }

        // 4)check if user change password after token wass issued
        if(freshUser.changePasswordAfter(decoded.iat)){
            return next() 
        }
        // Grant access to protected route
        // there is logged in user and send to template
        res.locals.user = freshUser
        // req.user = freshUser
        return next()
    }catch(err){
        return next()
    }
    }
    next()
})

exports.logOut = (req, res, next) => {
    res.cookie('jwt', 'loggedout', {
      expires: new Date(
        Date.now() + 10 * 1000
      ),
      httpOnly: true
    });
    res.status(200).json({
      status: 'success'
    });
  }