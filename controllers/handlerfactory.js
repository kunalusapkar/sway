const catchAsync = require('../util/catchAsync')
const Category = require('./../models/categorymodel')
const AppError = require('../util/appError')
const APIFeatures = require('./../util/apiFeature')

exports.deleteOne = Model => catchAsync(async(req,res,next)=>{
    const doc = await Model.findByIdAndDelete(req.params.id)
    if(!doc){
        return next(new AppError('No document found with that ID',404))
    }
    res.status(204).json({
        status:'success',
        data:null
    })
})

exports.updateOne = Model=> catchAsync(async (req,res,next)=>{
    const doc = await Model.findByIdAndUpdate(req.params.id,req.body,{
        new:true,
        runValidators:true
    })
    if(!doc){
        return next(new AppError('No document found with that ID',404))
    }
    res.status(200).json({
        status:'success',
        data:{
            data:doc
        }
})

})


exports.createOne = Model=>catchAsync(async (req,res,next)=>{
    const doc = await Model.create(req.body);
    res.status(201).json({
        status:'success',
        data:{
            data:doc
        }
    })
    
})

exports.getOne = (Model,popOptions) => catchAsync(async (req,res,next)=>{
    let query =  Model.findById(req.params.id)
    if(popOptions)query = query.populate(popOptions)
    
    const doc = await query
    if(!doc){
        return next(new AppError('No document found with that ID',404))
    }
    res.status(200).json({
        status:'success',
        results:doc.length,
        data:{
            tour:doc
        }
    })
    
})

exports.getAll = Model=> catchAsync(async(req,res,next)=>{

    // to allow for  reciews on 
    // execute query
    // console.log(req.query)
    var req_Data = req.query
    if(req.query.category_id){
        const main_cat_id = req.query.category_id
        const main_Category_Id = await Category.find({category_id:main_cat_id})
        console.log(main_Category_Id[0]._id)
        req_Data = {category:main_Category_Id[0]._id}
        console.log(req_Data)
    }
    const features = new APIFeatures(Model.find(),req_Data)
    .filter()
    .sort()
    .limitFields()
    .pagination()
    const doc = await features.query
    // const doc = await features.query.explain()
    res.status(200).json({
        status:'success',
        results:doc.length,
        data:doc
        
    })
 
});